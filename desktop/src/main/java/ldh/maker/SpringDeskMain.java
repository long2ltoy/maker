package ldh.maker;

import ldh.maker.component.SpringJavafxContentUiFactory;
import ldh.maker.util.UiUtil;

public class SpringDeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new SpringJavafxContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

