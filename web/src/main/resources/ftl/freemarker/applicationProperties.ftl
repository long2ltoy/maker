# datasource
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.url = jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull
spring.datasource.username = ${dbConnectionData.userNameProperty}
spring.datasource.password = ${dbConnectionData.passwordProperty}
spring.datasource.driverClassName = com.mysql.cj.jdbc.Driver
spring.datasource.maxActive = 25
spring.datasource.minIdle = 3
spring.datasource.initialSize = 10
spring.datasource.connectionTimeout = 30000
spring.datasource.maxWait = 30000
spring.datasource.minEvictableIdleTimeMillis = 30000

#设定ftl文件路径
spring.freemarker.template-loader-path=classpath:/templates,classpath:/ftl
spring.freemarker.allow-request-override=false
spring.freemarker.cache=false
spring.freemarker.check-template-location=true
spring.freemarker.charset=UTF-8
spring.freemarker.content-type=text/html
spring.freemarker.expose-request-attributes=false
spring.freemarker.expose-session-attributes=false
spring.freemarker.expose-spring-macro-helpers=false
spring.freemarker.prefer-file-system-access=false


spring.mvc.static-path-pattern=/**
spring.resources.static-locations=classpath:/
server.port=8080