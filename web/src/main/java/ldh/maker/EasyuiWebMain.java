package ldh.maker;

import ldh.maker.component.EasyuiContentUiFactory;
import ldh.maker.util.UiUtil;

public class EasyuiWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new EasyuiContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

