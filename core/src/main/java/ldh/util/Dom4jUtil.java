package ldh.util;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Dom4jUtil {

	public static Document load(String fileName) {
		if (fileName == null || fileName.trim().equals("")) {
			return null;
		}
		Document document = null;
		try {
			SAXReader saxReader = new SAXReader();
			document = saxReader.read(new File(fileName));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return document;
	}

	public static Map<String, String> select(Document document, String path) {
		Map<String, String> map = new HashMap<String, String>();
		if (document != null) {
			List<?> list = document.selectNodes(path);
			Iterator<?> it = list.iterator();
			while (it.hasNext()) {
				Element e = (Element) it.next();
				String javaType = e.attribute("javaType").getValue();
				String handler = e.attribute("handler").getValue();
				map.put(javaType, handler);
			}
		}
		return map;
	}
}
