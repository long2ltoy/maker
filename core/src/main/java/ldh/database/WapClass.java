package ldh.database;

/**
 * Created by ldh on 2017/4/19.
 */
public class WapClass {

    private String showText;
    private WapType wapType;
    private String name;

    public WapClass(String showText, WapType wapType) {
        this.showText = showText;
        this.wapType = wapType;
    }

    public String getShowText() {
        return showText;
    }

    public void setShowText(String showText) {
        this.showText = showText;
    }

    public WapType getWapType() {
        return wapType;
    }

    public void setWapType(WapType wapType) {
        this.wapType = wapType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enum WapType {
        TABLE,
        ENUM,

        ;
    }
}
