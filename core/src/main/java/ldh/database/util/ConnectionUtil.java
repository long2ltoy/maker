package ldh.database.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {

	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void close(Connection connection, ResultSet tableRet) {
		if (tableRet != null) {
			try {
				tableRet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		close(connection);
	}
	
	public static void close(Connection connection, Statement statement, ResultSet tableRet) {
		if (tableRet != null) {
			try {
				tableRet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		close(connection);
	}
}
