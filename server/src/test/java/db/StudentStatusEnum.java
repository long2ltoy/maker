package db;

import ldh.common.mybatis.ValuedEnum;

/**
 * Created by ldh on 2017/4/19.
 */
import ldh.common.mybatis.ValuedEnum;

public enum StudentStatusEnum implements ValuedEnum<Short> {
    Online((short)1),
    Offline((short)0),
    ;

    private Short value;

    private StudentStatusEnum(Short value){
        this.value = value;
    }

    @Override
    public Short getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return value + "";
    }
}

