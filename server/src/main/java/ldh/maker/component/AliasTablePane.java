package ldh.maker.component;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.database.TableInfo;
import ldh.maker.db.SettingDb;
import ldh.maker.db.TableViewDb;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;
import org.controlsfx.control.MaskerPane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ldh123 on 2018/5/5.
 */
public class AliasTablePane extends BaseSettingTabPane {

    private Map<String, TextField> textFieldMap = new HashMap<>();
    
    public AliasTablePane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

    protected String getTitle() {
        return "对表设置别名，可以进行关联查询";
    }

    protected Node initBody() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.getStyleClass().add("round-border");
        scrollPane.setPadding(new Insets(5, 5, 5, 5));

        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(10);

        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        int row = 0;
        int column = 0;
        for(Map.Entry<String, Table> entry : tableInfo.getTables().entrySet()) {
            Table table = entry.getValue();
            if (table.getPrimaryKey() == null || !table.isCreate()) continue;
            try {
                Label label = new Label(table.getName());
                label.setTooltip(new Tooltip(table.getName()));
                TextField textField = new TextField(table.getAlias());
                textFieldMap.put(table.getName(), textField);
                GridPane.setConstraints(label, column++, row);
                GridPane.setConstraints(textField, column++, row);
                gridPane.getChildren().addAll(label, textField);
                if (column == 6) {
                    column = 0;
                    row++;
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        gridPane.getColumnConstraints().addAll(buildLabel(160), buildNode(80), buildLabel(160), buildNode(80), buildLabel(160), buildNode(80));
        scrollPane.setContent(gridPane);
        return scrollPane;
    }

    protected void save(ActionEvent event) {
        if(!check()) return;
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        Map<String, TableViewData> tableViewDataMap = tableInfo.getTableViewDataMap();
        List<TableViewData> tableViewDataList = new ArrayList<>();
        for(Map.Entry<String, TextField> entry : textFieldMap.entrySet()) {
            Table table = tableInfo.getTable(entry.getKey());
            table.setAlias(entry.getValue().getText().trim());
            TableViewData tableViewData = tableViewDataMap.get(entry.getKey());
            if (tableViewData == null) {
                tableViewData = new TableViewData();
                tableViewData.setTreeNodeId(treeItem.getValue().getParent().getId());
                tableViewData.setDbName(dbName);
                tableViewData.setTableName(entry.getKey());
                tableViewDataMap.put(entry.getKey(), tableViewData);
            }
            tableViewData.setAlias(table.getAlias());
            tableViewDataList.add(tableViewData);
        }
        new Thread(new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                TableViewDb.save(tableViewDataList);
                return null;
            }
        }).start();
    }

    protected boolean check() {
        return true;
    }

    protected void reset(ActionEvent event) {
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        for(Map.Entry<String, TextField> entry : textFieldMap.entrySet()) {
            Table table = tableInfo.getTable(entry.getKey());
            entry.getValue().setText(table.getAlias());
        }
    }

    protected void clean(ActionEvent event) {
        for(Map.Entry<String, TextField> entry : textFieldMap.entrySet()) {
            entry.getValue().setText("");
        }
    }
}
