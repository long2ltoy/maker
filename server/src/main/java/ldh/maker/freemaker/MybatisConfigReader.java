package ldh.maker.freemaker;

import ldh.util.Dom4jUtil;
import org.dom4j.Document;

import java.util.Map;
import java.util.Map.Entry;

public class MybatisConfigReader {

	public static Map<String, String> getTypeHandlers() {
		String path = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\resources\\mybatis-config.xml";
		Document document = Dom4jUtil.load(path);
		Map<String, String> typeHandlers = Dom4jUtil.select(document, "/configuration/typeHandlers/typeHandler");
		return typeHandlers;
	}
	
	public static Map<String, String> getTypeHandlers(String path) {
		Document document = Dom4jUtil.load(path);
		Map<String, String> typeHandlers = Dom4jUtil.select(document, "/configuration/typeHandlers/typeHandler");
		return typeHandlers;
	}
	
	public static void main(String[] args) {
		Map<String, String> typeHandlers = getTypeHandlers();
		for (Entry<String, String> entry : typeHandlers.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
}
